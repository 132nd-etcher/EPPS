# coding=utf-8
from string import ascii_uppercase, digits
from tempfile import gettempdir

__author__ = 'etcher3rd'
import sys
from main import __version__
from random import choice, randint
# noinspection PyProtectedMember
from os import listdir, _exit, mkdir, startfile, environ
from os.path import join, dirname, abspath, exists
from json import dumps, loads
from PyQt5.QtCore import pyqtSlot, QObject, QTimer
from PyQt5.QtGui import QIcon, QKeySequence
from PyQt5.QtWidgets import QMainWindow, QApplication, QShortcut, QMessageBox, QPushButton, QDialog
from ui import ui_main as qt_main_ui, ui_update as qt_update_ui
from custom_logging import mkLogger, logged
from csv import excel, writer, reader, QUOTE_ALL
from pickle import load, dump
from requests import get as requests_get, head as requests_head
from urllib import request
from zipfile import ZipFile, BadZipfile, ZIP_LZMA
from shutil import rmtree, copy

cert = './cacert.pem'
environ['REQUESTS_CA_BUNDLE'] = cert

class Config():
    """
    Set it all up

    This holds everything that is accessible to the user, plus some more. It also writes/reads to the config file,
    so nothing's lost between runs.
    """
    defaults = {
        'qty': 7,
        'difficulty': 0,
        'csv': 0,
    }

    def __init__(self):
        self.__dict__['config'] = {}
        self.__dict__['path'] = None
        if hasattr(sys, 'frozen'):
            self.path = join(dirname(abspath(sys.executable)), 'epps.config')
        else:
            self.path = join(dirname(abspath(__file__)), 'epps.config')
        self.read()

    def read(self):
        if exists(self.path):
            with open(self.path) as f:
                self.__dict__['config'] = loads(f.read())

    def write(self):
        with open(self.path, mode='w') as f:
            f.write(dumps(self.__dict__['config'], indent=True, sort_keys=True))

    def __getattr__(self, key):
        for d in [self.__dict__, self.__dict__['config'], self.defaults]:
            try:
                return d[key]
            except KeyError:
                pass
        return ''

    def __setattr__(self, key, value):
        if key in self.__dict__.keys():
            self.__dict__[key] = value
        else:
            self.__dict__['config'][key] = value
            self.write()

    def __getitem__(self, key):
        return getattr(self, key)

    def __setitem__(self, key, value):
        self.__setattr__(key, value)


class DataSet():
    def __init__(self, init_list=None):
        if init_list:
            self.l = list(init_list)
        else:
            self.l = []

    def __len__(self):
        return len(self.l)

    # __iter__ is not strictly required, it's only needed to implement
    # efficient iteration.
    def __iter__(self):
        return self.l.__iter__()

    # __contains__ isn't strictly required either, it's only needed to
    # implement the `in` operator efficiently.
    def __contains__(self, item):
        return self.l.__contains__(item)

    def __getitem__(self, item):
        return self.l.__getitem__(item)

    # Mutable sequences only, provide the Python list methods.
    def append(self, item):
        self.l.append(item)

    def count(self, item):
        return self.l.count(item)

    def index(self, item):
        return self.l.index(item)

    def extend(self, other):
        return self.l.extend(other)

    def insert(self, index, item):
        return self.l.insert(index, item)

    def pop(self):
        return self.l.pop()

    def remove(self, item):
        return self.l.remove(item)

    def reverse(self):
        return self.l.reverse()

    def __eq__(self, other):
        return self.l == other.l

    def __str__(self):
        return self.l.__str__()

    def sort(self):
        self.l.sort()
        return self


class CSV(DataSet):
    csv_dialect = excel
    csv_dialect.lineterminator = '\n'
    csv_dialect.delimiter = ';'
    csv_dialect.quoting = QUOTE_ALL

    def __init__(self, target_file):
        """
        Superseeds data_set.DataSet with methods for reading / writing from / to CSV files

        :param target_file: CSV file to read or write
        """
        DataSet.__init__(self)
        self.target_file = target_file

    # def get_target_file(self):
    # return self.__target_file

    # def set_target_file(self, value):
    # self.__target_file = value
    #
    # def del_target_file(self):
    # del self.__target_file

    def read(self):
        with open(self.target_file) as f:
            data = reader(f, self.csv_dialect)
            for row in data:
                self.l.append(row)
        return self

    def write(self):
        with open(self.target_file, 'w') as csvFile:
            data = writer(csvFile, dialect=self.csv_dialect)
            for row in self.l:
                data.writerow(row)
        return self

        # target_file = property(get_target_file, set_target_file, del_target_file, "CSV file to read or write")


class Stats(object):
    """
    classdocs
    """
    d = {}

    def __init__(self):
        """
        Constructor
        """
        self.file_name = 'epps.stats'

    def read(self):
        if not exists(self.file_name):
            self.d = {'total': {'wrong': 0, 'correct': 0}}
            self.write()
        with open(self.file_name, 'rb') as f:
            # noinspection PyArgumentList
            self.d = load(f)

    def write(self):
        with open(self.file_name, 'wb') as f:
            # noinspection PyArgumentList
            dump(self.d, f)

    def correct(self, question):
        if question in self.d.keys():
            self.d[question]['correct'] += 1
        else:
            self.d[question] = {'correct': 1, 'wrong': 0}
        self.d['total']['correct'] += 1
        self.write()

    def wrong(self, question):
        if question in self.d.keys():
            self.d[question]['wrong'] += 1
        else:
            self.d[question] = {'correct': 0, 'wrong': 1}
        self.d['total']['wrong'] += 1
        self.write()

    def get_wrongs(self, percentile=25):
        qty = len(self.d) * (float(percentile) / 100)
        rtn = []
        for k in sorted([(x, self.d[x]['wrong']) for x in self.d.keys() if x != 'total'], key=lambda y: y[1],
                        reverse=True):
            rtn.append(k[0])
            if len(rtn) >= qty:
                return rtn

    def get_corrects(self, percentile=25):
        qty = len(self.d) * (float(percentile) / 100)
        rtn = []
        for k in sorted([(x, self.d[x]['correct']) for x in self.d.keys() if x != 'total'], key=lambda y: y[1],
                        reverse=True):
            rtn.append(k[0])
            if len(rtn) >= qty:
                return rtn

    def get_total_correct(self):
        return self.d['total']['correct']

    def get_total_wrong(self):
        return self.d['total']['wrong']

    def __str__(self):
        return 'Bon: {}\tMauvais: {}'.format(self.get_total_correct(), self.get_total_wrong())


def humansize(nbytes):
    suffixes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB']
    if nbytes == 0:
        return '0 B'
    i = 0
    while nbytes >= 1024 and i < len(suffixes) - 1:
        nbytes /= 1024.
        i += 1
    f = ('%.2f' % nbytes).rstrip('0').rstrip('.')
    return '%s %s' % (f, suffixes[i])


class Gui():
    def __init__(self):
        pass

    class UpdateEPPS(QDialog, qt_update_ui.Ui_Dialog):
        class UpdateProgressBar():
            def __init__(self, parent):
                self.progress_bar = parent.progress_bar

            def update_title(self, title):
                pass

            def update(self, i):
                self.progress_bar.setValue(round(int(i)))

        def __init__(self):
            logger.debug("UpdateGui - init")
            QDialog.__init__(self)
            self.setupUi(self)
            self.setWindowTitle('Mise à jour d\'EPPS')
            self.setModal(True)
            self.setWindowIcon(icon)
            self.download_progress_bar = Gui.UpdateEPPS.UpdateProgressBar(self)

    class Main(QMainWindow, QObject, qt_main_ui.Ui_MainWindow):

        logger, csv, qa_pair = None, None, None

        @logged
        def __init__(self):
            QObject.__init__(self)
            self.config = Config()
            self.setupUi(self)
            self.setWindowTitle('EPPS {}'.format(__version__))
            self.ensurePolished()
            self.timer = QTimer()
            self.style_timer = QTimer()
            self.update_gui = Gui.UpdateEPPS()
            # noinspection PyUnresolvedReferences
            self.style_timer.timeout.connect(self.on_style_timer_timeout)
            self.csv_combo.addItems([x[:-4] for x in listdir('.') if x[-4:] == '.csv'])
            # noinspection PyUnresolvedReferences
            self.csv_combo.currentIndexChanged.connect(self.on_csv_combo_index_changed)
            self.buttons = {
                0: self.toolButton_1,
                1: self.toolButton_2,
                2: self.toolButton_3,
                3: self.toolButton_4,
                4: self.toolButton_5,
                6: self.toolButton_6,
                5: self.toolButton_7,
                7: self.toolButton_8,
                8: self.toolButton_9,
                9: self.toolButton_10,
            }
            self.shortcuts = []
            for i in self.buttons.items():
                i[1].setFixedHeight(40)
                i[1].setFixedWidth(700)
                s = QShortcut(QKeySequence(str(i[0])), self)
                # noinspection PyUnresolvedReferences
                s.activated.connect(self.buttons[i[0]].click)
                self.shortcuts.append(s)
            self.stats = Stats()
            self.stats.read()
            self.refresh_stats()
            self.config_values = [
                (self.difficulty_combo, 'difficulty'),
                (self.csv_combo, 'csv'),
                (self.choices_qty_combo, 'qty')
            ]
            for x in self.config_values:
                x[0].setCurrentIndex(self.config[x[1]])
                # noinspection PyUnresolvedReferences
                x[0].currentIndexChanged.connect(lambda iii=x[0], ii=x[1]: self.on_config_value_update(iii, ii))
            # noinspection PyUnresolvedReferences
            self.start_button.clicked.connect(self.start)
            # noinspection PyUnresolvedReferences
            self.timer.timeout.connect(self.on_timer_update)
            self.time_max, self.time_left = None, None
            self.default_style = self.styleSheet()
            self.check_for_new_version()
            self.show()

        @pyqtSlot()
        def on_timer_update(self):
            self.time_left -= 100
            if self.time_left <= 0:
                self.on_wrong_answer()
            else:
                self.progress_bar.setValue(self.time_left / self.time_max * 100)

        @pyqtSlot()
        def on_config_value_update(self, idx, value):
            self.config[value] = idx

        @pyqtSlot()
        def on_csv_combo_index_changed(self):
            self.load_csv()

        def load_csv(self):
            self.logger.debug(self.csv_combo.currentText())
            self.csv = CSV('{}.csv'.format(self.csv_combo.currentText()))
            self.csv.read()

        def start(self):
            self.load_csv()
            self.make_question()

        def make_question(self):
            qi, ai = 0, 1
            if self.inversed:
                qi, ai = 1, 0
            self.qa_pair = tuple(choice(self.csv))
            if self.prio_wrong:
                self.qa_pair = tuple(choice(self.stats.get_wrongs()))
            q, a = self.qa_pair[qi], self.qa_pair[ai]
            self.question_label.setText(q)
            ca = randint(0, self.choices_qty - 1)
            wa = [choice(self.csv)[ai] for _ in range(self.choices_qty)]
            while a in wa:
                wa.remove(a)
                wa.append(choice(self.csv)[ai])
            for k in self.buttons:
                try:
                    # noinspection PyUnresolvedReferences
                    self.buttons[k].clicked.disconnect()
                    self.buttons[k].setText('')
                except TypeError:
                    pass
            for i in range(0, self.choices_qty):
                if i == ca:
                    self.buttons[i].setText('{}: {}'.format(i, a))
                    # noinspection PyUnresolvedReferences
                    self.buttons[i].clicked.connect(self.on_correct_answer)
                else:
                    self.buttons[i].setText('{}: {}'.format(i, wa.pop()))
                    # noinspection PyUnresolvedReferences
                    self.buttons[i].clicked.connect(self.on_wrong_answer)
            if self.difficulty_combo.currentIndex() > 0:
                self.time_max = (10000 + self.choices_qty * 1000) / self.difficulty
                self.time_left = self.time_max
                self.timer.start(100)
            elif self.timer.isActive():
                self.timer.stop()
                self.progress_bar.setValue(0)

        @pyqtSlot()
        def on_style_timer_timeout(self):
            self.setStyleSheet(self.default_style)

        @pyqtSlot()
        def on_wrong_answer(self):
            self.stats.wrong(self.qa_pair)
            self.next_question('red')

        @pyqtSlot()
        def on_correct_answer(self):
            self.stats.correct(self.qa_pair)
            self.next_question('green')

        def next_question(self, color=None):
            if color is not None:
                self.setStyleSheet('background-color: {}'.format(color))
                self.style_timer.start(100)
            self.make_question()
            self.refresh_stats()

        def refresh_stats(self):
            self.stats_label.setText(str(self.stats))

        @property
        def inversed(self):
            return self.inversed_checkbox.isChecked()

        @property
        def prio_wrong(self):
            return self.priowrong_checkbox.isChecked()

        @property
        def choices_qty(self):
            return int(self.choices_qty_combo.currentText())

        @property
        def difficulty(self):
            return self.difficulty_combo.currentIndex()

        def check_for_new_version(self):
            # if not hasattr(sys, 'frozen'):
            #     return
            self.logger.debug("vérification de l'existence d'une nouvelle version d'EPPS")
            r = requests_get('https://api.github.com/repos/etcher3rd/EPPS/releases')
            if not r:
                self.logger.error("erreur lors de la requete HTTP")
                return
            # noinspection PyBroadException
            try:
                for x in r.json():
                    tag = x['tag_name']
                    url = x['assets'][0]['browser_download_url']
                    size = x['assets'][0]['size']
                    notes = x['body']
                    draft = x['draft']
                    prerelease = x['prerelease']
                    logger.debug(notes)
                    logger.debug(draft)
                    logger.debug(prerelease)
                    # skip all alpha & beta versions if we're not already on it
                    if 'alpha' in tag and not 'alpha' in __version__ \
                            or 'beta' in tag and not 'beta' in __version__:
                        continue
                    # we're already up to date
                    elif tag == __version__:
                        return
                    # return latest version
                    # return url, size, notes, tag
                    if self.confirm("<p>Une nouvelle version d'EKPI est disponible: {}</p>"
                                    "<p>Voulez-vous mettre à jour maintenant ?<br>"
                                    "(EKPI sera automatiquement redemarré)</p>"
                                    "<p>Notes de version: {}</p>".format(tag, notes)):
                        # self.hide()
                        self.update_gui.show()
                        tmp_file = abspath(join(gettempdir(), ''.join(choice("{0}{1}".format(
                            ascii_uppercase, digits)) for _ in range(15))))
                        if not download(url, tmp_file, size=size, download_nice_name='EPPS',
                                        callback=self.update_gui.download_progress_bar):
                            self.logger.error('erreur lors du téléchargement')
                            return
                        if exists('./update'):
                            rmtree('./update')
                        mkdir('./update')
                        if not unzip(tmp_file, './update'):
                            self.logger.error('erreur lors de la décompression')
                        self.logger.debug("fermeture du programme")
                        for f in listdir('./update'):
                            if f == "epps.exe":
                                continue
                            try:
                                copy('./update/{}'.format(f), f)
                            except:
                                continue
                        rmtree('./update')
                        self.update_gui.hide()
                        startfile('epps.exe')
                        _exit(0)
                    else:
                        return

            except IndexError:
                self.logger.error('erreur lors de la recherche de mise à jour; la dernière release n\'a pas encore de '
                                  'fichier disponible au téléchargement, probablement qu\'etcher est en train d\''
                                  'uploader, ou qu\'il a retiré la release à cause d\'un bug découvert à la dernière '
                                  'minute. Vous pourrez réessayer d\'ici quelques minutes.')
            except:
                self.logger.exception("erreur lors de la recherche d'une nouvelle version")
                return
            self.logger.debug("pas de nouvelle version")
            return

        @staticmethod
        def __build_msg_box():
            _msgbox = QMessageBox()
            _msgbox.setWindowTitle("EPPS {}".format(__version__))
            _msgbox.setWindowIcon(icon)
            _msgbox.setTextFormat(1)
            return _msgbox

        @pyqtSlot(str)
        def msgbox(self, text):
            _msgbox = self.__build_msg_box()
            _msgbox.setText(text)
            _msgbox.addButton(QPushButton('Ok ...'), QMessageBox.YesRole)
            _msgbox.exec_()

        @pyqtSlot(str)
        def confirm(self, text):
            _msgbox = self.__build_msg_box()
            _msgbox.setText(text)
            _msgbox.addButton(QMessageBox.Yes)
            _msgbox.addButton(QMessageBox.No)
            if _msgbox.exec_() == QMessageBox.Yes:
                return True
            return


def download(url, target, count=1, size=None, download_nice_name='', callback=None, redirect=False):
    logger.debug("Defer - download - début du téléchargement")
    logger.debug("Defer - download - url: {}".format(url))
    logger.debug("Defer - download - fichier local: {}".format(target))
    try:
        logger.debug("Defer - download - récupération des headers")
        resp = requests_head(url)
    except:
        logger.exception("Defer - download - erreur lors de la récupération des headers")
        return
    if not resp:
        logger.exception("Defer - download - erreur lors de la récupération des headers")
        return
    logger.debug('Defer - download - lecture des headers')
    try:
        h = resp.headers
        if size is None:
            logger.debug('Defer - download - recherche de la taille du fichier à télécharger dans les headers')
            if 'Content-Length' in h.keys():
                size = h['Content-Length']
                logger.debug('Defer - download - taille trouvée: {}'.format(size))
    except:
        logger.error('defer-download - erreur lors de la lecture des headers')
        return
    if redirect and 'location' in h.keys() and not h['location'] == url:
        logger.debug('Defer - download - redirection vers la nouvelle url: {}'.format(h['location']))
        if count > 10:
            logger.error("Defer - download - trop de redirections, je laisse tomber")
            return
        return download(h['location'], target, count + 1, size=size,
                        download_nice_name=download_nice_name, redirect=redirect)
    if size is None:
        if count > 10:
            logger.error('Defer - download - je ne suis pas parvenu à obtenir la taille du fichier distant')
            logger.error('Defer - download - headers: {}'.format(h))
            return
        return download(url, target, count=count + 1, size=size,
                        download_nice_name=download_nice_name, callback=callback, redirect=redirect)
    else:
        size = int(size)
    logger.debug("Defer - download - début du transfert des données")
    try:
        callback.update(0)
        callback.update_title("Téléchargement de : {} ({})".format(download_nice_name, humansize(size)))
        with request.urlopen(url) as resp, open(target, mode='wb') as f:
            size_dl = 0
            while True:
                buffer = resp.read(512)
                if not buffer:
                    break
                size_dl += len(buffer)
                f.write(buffer)
                callback.update(size_dl * 100 / size)
    except:
        logger.exception("Defer - download - erreur lors du transfert des données")
        return
    callback.update_title("Téléchargement de : {} ({}) - succès".format(download_nice_name, humansize(size)))
    callback.update(100)
    return True


def unzip(zip_file_path, target_dir):
    logger.debug("Defer - unzip - ouverture du fichier ZIP")
    try:
        with ZipFile(zip_file_path, mode='r', compression=ZIP_LZMA) as zip_file:
            logger.debug(
                "Defer - unzip - extraction des données vers {}".format(target_dir))
            zip_file.extractall(target_dir)
    except BadZipfile:
        logger.exception(
            "il semble que le fichier ZIP suivant soit corrompu: {}".format(zip_file_path))
        return
    except:
        logger.exception("Defer - unzip- erreur lors de l'extraction du fichier ZIP")
        logger.error("Est-ce que le dossier suivant est protégé par l'UAC Windows ? \n\"{}\""
                     "\n\nSi oui, il faudrait peut-être redémarrer EKPI en mode administrateur"
                     .format(zip_file_path))
        return
    return True


logger = mkLogger('__main__')
qt_app = QApplication(sys.argv)
icon = QIcon(':/ico/epps.ico')
ui_main = Gui.Main()
ui_main.setWindowIcon(icon)
_exit(qt_app.exec())
