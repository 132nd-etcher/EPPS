# coding=utf-8
__author__ = 'etcher3rd'
__version__ = "alpha1"
__guid__ = '9d2b1103-1191-4b5f-921f-01de52edd988'

import sys
import ctypes
from os import _exit

if __name__ == "__main__":
    if hasattr(sys, 'frozen'):
        sys.stdout = open("stdout.log", "w")
        sys.stderr = open("stderr.log", "w")
        try:
            ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(__guid__)
        except:
            pass
    import epps
    _exit(0)